from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {"todos": todos}
    return render(request, "todos/todo_lists/list.html", context)


def todo_list_detail(request, id):
    list = TodoList.objects.get(id=id)
    context = {"list": list}
    return render(request, "todos/todo_lists/detail.html", context)


def todo_list_delete(request, id):
    if request.method == "POST":
        list = TodoList.objects.get(id=id)
        list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/todo_lists/delete.html")


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            create = form.save()
            return redirect("todo_list_detail", id=create.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/todo_lists/create.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            create = form.save()
            return redirect("todo_list_detail", id=create.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/todo_items/create.html", context)


def todo_list_update(request, id):
    update = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=update)
        if form.is_valid():
            update = form.save()
            return redirect("todo_list_detail", id=update.id)
    else:
        form = TodoListForm(instance=update)
    context = {"form": form}
    return render(request, "todos/todo_lists/update.html", context)


def todo_item_update(request, id):
    update = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=update)
        if form.is_valid():
            update = form.save()
            return redirect("todo_list_detail", id=update.id)
    else:
        form = TodoItemForm(instance=update)
    context = {"form": form}
    return render(request, "todos/todo_items/update.html", context)
